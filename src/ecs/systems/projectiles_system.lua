---
-- projectiles_system.lua


local class = require "middleclass"
local Timer = require "hump.timer"

local EntityFactory = require "utils.factories.entity_factory"


local ProjectilesSystem = class("ProjectilesSystem", System)

function ProjectilesSystem:initialize(data)
    System.initialize(self)

    self.entity_factory = EntityFactory()
    self.timer = Timer.new()

    signal:register("aim-cahin", function (aim_chain) self:_create_projectile(aim_chain) end)

    self.chain = {}
    self.chain_position = 1
end

function ProjectilesSystem:update(dt)
    self.timer:update(dt)
end

function ProjectilesSystem:requires()
    return {}
end

function ProjectilesSystem:onAddEntity(entity)

end

function ProjectilesSystem:onRemoveEntity(entity)
    -- body
end

function ProjectilesSystem:_create_projectile(aim_chain)
    for _, point in ipairs(aim_chain) do
        local pos = {}
        pos.x = point:get("Position"):get_x()
        pos.y = point:get("Position"):get_y()

        table.insert(self.chain, pos)
    end

    local x, y = aim_chain[1]:get("Position"):get()
    local projectile = self.entity_factory:get_projectile(x, y)

    engine:addEntity(projectile)

    self.timer:every(0.02, function () return self:_handle_projectiles() end)
end

function ProjectilesSystem:_handle_projectiles()
    self.chain_position = self.chain_position + 1

    if self.chain_position > #self.chain then
        self.chain = {}
        self.chain_position = 1

        local projectiles = engine:getEntitiesWithComponent("ProjectileEntity")

        for _, projectile in pairs(projectiles) do
            engine:removeEntity(projectile)
        end

        signal:emit("fire-end")

        return false
    end

    local projectiles = engine:getEntitiesWithComponent("ProjectileEntity")

    for _, projectile in pairs(projectiles) do
        local cur_x = self.chain[self.chain_position].x
        local cur_y = self.chain[self.chain_position].y

        projectile:get("Position"):set(cur_x, cur_y)

        if self.chain_position == #self.chain then
            projectile:get("Tile"):set("hit")
        end
    end
end

return ProjectilesSystem
