---
-- base_view_system.lua


local class = require "middleclass"


local BaseViewSystem = class("BaseViewSystem", System)

function BaseViewSystem:initialize(data)
    System.initialize(self)

    self.tile_size = 32
    self.tileset_width = nil
    self.tileset_height = nil
end

function BaseViewSystem:draw()
    for key, entity in pairs(self.targets) do
        local x, y = entity:get("Position"):get()

        love.graphics.draw(
            self.tileset,
            self.tiles[entity:get("Tile"):get()],
            x * self.tile_size - self.tile_size,
            y * self.tile_size - self.tile_size
        )
    end
end

function BaseViewSystem:_add_tile(name, x, y)
    self.tiles[name] = love.graphics.newQuad(
        x * self.tile_size - self.tile_size,
        y * self.tile_size - self.tile_size,
        self.tile_size,
        self.tile_size,
        self.tileset_width,
        self.tileset_height
    )
end

function BaseViewSystem:_set_tileset(tileset_path)
    self.tileset = love.graphics.newImage(tileset_path)
    self.tileset_width = self.tileset:getWidth()
    self.tileset_height = self.tileset:getHeight()
end

return BaseViewSystem
