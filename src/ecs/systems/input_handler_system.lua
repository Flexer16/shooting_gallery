---
-- input_handler_system.lua


local class = require "middleclass"

local FSM = require "utils.fsm.fsm"
local PlayerMovingState = require "game_logic.player_moving_state"
local AimState = require "game_logic.aim_state"
local FireState = require "game_logic.fire_state"


local InputHandlerSystem = class("InputHandlerSystem", System)

function InputHandlerSystem:initialize(data)
    System.initialize(self)

    self.fsm = FSM({owner = self})

    self.states = {
        player_moving = PlayerMovingState(),
        aim = AimState(),
        fire = FireState()
    }

    self.fsm:set_current_state(self.states.player_moving)
end

function InputHandlerSystem:update(dt)
    self.fsm:update(dt)
end

function InputHandlerSystem:requires()
    return {}
end

function InputHandlerSystem:onAddEntity(entity)
    -- body
end

function InputHandlerSystem:onRemoveEntity(entity)
    -- body
end

function InputHandlerSystem:get_fsm()
    return self.fsm
end

function InputHandlerSystem:get_states()
    return self.states
end

return InputHandlerSystem
