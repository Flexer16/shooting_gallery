---
-- view_projectiles_system.lua


local class = require "middleclass"

local BaseViewSystem = require "ecs.systems.base_view_system"


local ViewProjectilesSystem = class("ViewProjectilesSystem", BaseViewSystem)

function ViewProjectilesSystem:initialize(data)
    BaseViewSystem.initialize(self)

    self:_set_tileset("res/tiles/tileset02.png")

    self.tiles = {}

    self:_set_tiles()
end

function ViewProjectilesSystem:requires()
    return {"ProjectileEntity"}
end

function ViewProjectilesSystem:onAddEntity(entity)
    -- body
end

function ViewProjectilesSystem:onRemoveEntity(entity)
    -- body
end

function ViewProjectilesSystem:_set_tiles()
    self:_add_tile("flight", 2, 9)
    self:_add_tile("hit", 1, 9)
end

return ViewProjectilesSystem
