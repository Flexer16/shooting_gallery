---
-- view_creatures_system.lua


local class = require "middleclass"

local BaseViewSystem = require "ecs.systems.base_view_system"


local ViewCreaturesSystem = class("ViewCreaturesSystem", BaseViewSystem)

function ViewCreaturesSystem:initialize(data)
    BaseViewSystem.initialize(self)

    self:_set_tileset("res/tiles/tileset02.png")

    self.tiles = {}

    self:_set_tiles()
end

function ViewCreaturesSystem:requires()
    return {"CreatureEntity"}
end

function ViewCreaturesSystem:onAddEntity(entity)
    -- body
end

function ViewCreaturesSystem:onRemoveEntity(entity)
    -- body
end

function ViewCreaturesSystem:_set_tiles()
    self:_add_tile("@", 1, 6)
end

return ViewCreaturesSystem
