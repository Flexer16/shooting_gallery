---
-- view_aim_system.lua


local class = require "middleclass"

local BaseViewSystem = require "ecs.systems.base_view_system"


local ViewAimSystem = class("ViewAimSystem", BaseViewSystem)

function ViewAimSystem:initialize(data)
    BaseViewSystem.initialize(self)

    self:_set_tileset("res/tiles/tileset02.png")

    self.tiles = {}

    self:_set_tiles()
end

function ViewAimSystem:requires()
    return {"AimEntity"}
end

function ViewAimSystem:onAddEntity(entity)
    -- body
end

function ViewAimSystem:onRemoveEntity(entity)
    -- body
end

function ViewAimSystem:_set_tiles()
    self:_add_tile("final", 2, 8)
    self:_add_tile("regular", 1, 8)
    self:_add_tile("negative", 3, 8)
end

return ViewAimSystem
