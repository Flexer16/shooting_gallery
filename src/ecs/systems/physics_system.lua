---
-- physics_system.lua


local class = require "middleclass"
local bump = require "bump.bump"

local CoordinateConverter = require "utils.helpers.coordinate_converter"
local PhysicsComponentTypes = require "utils.enums.physics_component_type"


local PhysicsSystem = class("PhysicsSystem", System)

function PhysicsSystem:initialize(data)
    System.initialize(self)

    self.cell_size = 32
    self.world = bump.newWorld(self.cell_size)

    self.coordinate_converter =  CoordinateConverter({cell_size = self.cell_size})
end

function PhysicsSystem:update(dt)
    for key, entity in pairs(self.targets) do
        if entity:get("PhysicsEntity"):is_type(PhysicsComponentTypes.dynamic) then
            if entity:get("PhysicsEntity"):is_target() then
                local local_target_x, local_target_y = entity:get("PhysicsEntity"):remove_target()

                local actual_x, actual_y = self.world:move(
                    entity,
                    self.coordinate_converter:get_world_pos(local_target_x),
                    self.coordinate_converter:get_world_pos(local_target_y)
                )

                entity:get("Position"):set(self.coordinate_converter:get_local_pos(actual_x), self.coordinate_converter:get_local_pos(actual_y))
            end
        end
    end
end

function PhysicsSystem:requires()
    return {"PhysicsEntity"}
end

function PhysicsSystem:onAddEntity(entity)
    local x, y = entity:get("Position"):get()
    local world_x = self.coordinate_converter:get_world_pos(x)
    local world_y = self.coordinate_converter:get_world_pos(y)

    self.world:add(entity, world_x, world_y, self.cell_size, self.cell_size)
end

function PhysicsSystem:onRemoveEntity(entity)
    -- body
end

return PhysicsSystem
