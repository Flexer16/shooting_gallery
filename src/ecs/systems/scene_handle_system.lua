---
-- scene_handle_system.lua


local class = require "middleclass"

local MapData = require "utils.helpers.map_data"
local MapTileParser = require "utils.helpers.map_tile_parser"


local SceneHandleSystem = class("SceneHandleSystem", System)

function SceneHandleSystem:initialize(data)
    System.initialize(self)

    signal:register("load-new-scene", function (file_path) self:_load_scene(file_path) end)
end

function SceneHandleSystem:update(dt)
    -- body
end

function SceneHandleSystem:requires()
    return {}
end

function SceneHandleSystem:onAddEntity(entity)
    -- body
end

function SceneHandleSystem:onRemoveEntity(entity)
    -- body
end

function SceneHandleSystem:_load_scene(file_path)
    local map_tile_parser = MapTileParser()
    local map_data = MapData({file_path = file_path})
    local map_str = map_data:get_map_string()

    local x, y = 1, 1

    for row in map_str:gmatch("[^\n]+") do
        x = 1

        for tile in row:gmatch(".") do
            map_tile_parser:parse(tile, x, y)

            x = x + 1
        end

        y = y + 1
    end

    signal:emit("beautifire-map")
end

return SceneHandleSystem
