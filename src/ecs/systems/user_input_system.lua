---
-- user_input_system.lua


local class = require "middleclass"
local Input = require "input"

local Actions = require "utils.enums.actions"

local UserInputSystem = class("UserInputSystem", System)

function UserInputSystem:initialize(data)
    System.initialize(self)

    self.input = Input()

    self.key_table = {
        {"up", Actions.moving_up},
        {"down", Actions.moving_down},
        {"left", Actions.moving_left},
        {"right", Actions.moving_right},
        {"f", Actions.fire},
        {"escape", Actions.escape}
    }

    self:_set_key_table()
end

function UserInputSystem:update(dt)
    for _, val in ipairs(self.key_table) do
        if self.input:down(val[2], 0.10) then
            signal:emit("kay-action", val[2])
        end
    end
end

function UserInputSystem:requires()
    return {}
end

function UserInputSystem:onAddEntity(entity)
    -- body
end

function UserInputSystem:onRemoveEntity(entity)
    -- body
end

function UserInputSystem:_set_key_table()
    for _, val in ipairs(self.key_table) do
        self.input:bind(val[1], val[2])
    end
end

return UserInputSystem
