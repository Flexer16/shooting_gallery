---
-- aim_handler_system.lua


local class = require "middleclass"

local Matrix = require "utils.other.matrix"
local EntityFactory = require "utils.factories.entity_factory"
local Bresenham = require "utils.helpers.bresenham"
local AimTypes = require "utils.enums.aim_types"
local MapTypes = require "utils.enums.map_types"


local AimHandlerSystem = class("AimHandlerSystem", System)

function AimHandlerSystem:initialize(data)
    System.initialize(self)

    self.entity_factory = EntityFactory()

    self.world = Matrix(100, 100)
    self.finish_aim = nil

    self.aim_chain = {}

    signal:register("clear-aim-data", function () self:_clear_aim_data() end)
    signal:register("create-aim-data", function () self:_create_aim() end)
    signal:register("move-aim", function (dx, dy) self:_move_aim(dx, dy) end)
    signal:register("send-aim-cahin", function () self:_send_aim_chain() end)
end

function AimHandlerSystem:update(dt)
    -- body
end

function AimHandlerSystem:draw()
    -- body
end

function AimHandlerSystem:requires()
    return {"MapEntity"}
end

function AimHandlerSystem:onAddEntity(entity)
    local x, y = entity:get("Position"):get()
    self.world:set(x, y, entity)
end

function AimHandlerSystem:onRemoveEntity(entity)
    -- body
end

function AimHandlerSystem:_clear_aim_data()
    local aim_entity_list = engine:getEntitiesWithComponent("AimEntity")

    for _, aim_entity in pairs(aim_entity_list) do
        engine:removeEntity(aim_entity)
    end

    self.finish_aim = nil
end

function AimHandlerSystem:_create_aim()
    local player_list = engine:getEntitiesWithComponent("Player")

    for _, player in pairs(player_list) do
        local x, y = player:get("Position"):get()

        if not self.finish_aim then
            self.finish_aim = self.entity_factory:get_finish_aim_entity(x, y)
            engine:addEntity(self.finish_aim)
        end
    end
end

function AimHandlerSystem:_move_aim(dx, dy)
    local x, y = self.finish_aim:get("Position"):get()

    if self.world:check(x + dx, y + dy) then
        self:_clear_aim_chain()

        self.finish_aim:get("Position"):set(x + dx, y + dy)

        self:_calculate_aim_chain()
    end
end

function AimHandlerSystem:_calculate_aim_chain()
    local bresenham = Bresenham()

    local aim_entity
    local is_blocked = false

    local aim_chain_adding = function (x, y)
        if not is_blocked then
            if self.world:get(x, y):get("MapEntity"):is_type(MapTypes.wall) then
                aim_entity = self.entity_factory:get_negative_aim_entity(x, y)
                is_blocked = true
            else
                aim_entity = self.entity_factory:get_regular_aim_entity(x, y)
            end

            table.insert(self.aim_chain, aim_entity)
        else
            aim_entity = self.entity_factory:get_negative_aim_entity(x, y)
        end

        engine:addEntity(aim_entity)
    end

    local player_list = engine:getEntitiesWithComponent("Player")

    for _, player in pairs(player_list) do
        local start_x, start_y = player:get("Position"):get()
        local end_x, end_y = self.finish_aim:get("Position"):get()

        bresenham:line(aim_chain_adding, start_x, start_y, end_x, end_y)
    end
end

function AimHandlerSystem:_clear_aim_chain()
    local aim_entity_list = engine:getEntitiesWithComponent("AimEntity")

    for _, aim_entity in pairs(aim_entity_list) do
        if aim_entity:get("AimEntity"):is_type(AimTypes.regular_aim) or aim_entity:get("AimEntity"):is_type(AimTypes.negative_aim) then
            engine:removeEntity(aim_entity)
        end
    end

    self.aim_chain = {}
end

function AimHandlerSystem:_send_aim_chain()
    if #self.aim_chain > 1 then
        signal:emit("aim-cahin", self.aim_chain)
        signal:emit("chain-send-success")

        self:_clear_aim_chain()
    end
end

return AimHandlerSystem
