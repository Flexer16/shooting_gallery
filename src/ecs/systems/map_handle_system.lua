---
-- map_handle_system.lua


local class = require "middleclass"

local MapTypes = require "utils.enums.map_types"


local MapHandleSystem = class("MapHandleSystem", System)

function MapHandleSystem:initialize(data)
    System.initialize(self)

    self.wall_list = {}
    self.floor_list = {}

    signal:register("beautifire-map", function () self:_beautifire_map() end)
end

function MapHandleSystem:update(dt)
    -- body
end

function MapHandleSystem:requires()
    return {"MapEntity"}
end

function MapHandleSystem:onAddEntity(entity)
    if entity:get("MapEntity"):is_type(MapTypes.wall) then
        table.insert(self.wall_list, entity)
    end

    if entity:get("MapEntity"):is_type(MapTypes.floor) then
        table.insert(self.floor_list, entity)
    end
end

function MapHandleSystem:onRemoveEntity(entity)
    -- body
end

function MapHandleSystem:_beautifire_map()
    for _, wall in ipairs(self.wall_list) do
        local neigh_data = 0
        local cur_x, cur_y = wall:get("Position"):get()

        for _, finder_wall in ipairs(self.wall_list) do
            local finder_x, finder_y = finder_wall:get("Position"):get()

            if finder_x == cur_x and finder_y == cur_y - 1 then
                neigh_data = neigh_data + 1
            end

            if finder_x == cur_x and finder_y == cur_y + 1 then
                neigh_data = neigh_data + 8
            end

            if finder_x == cur_x - 1 and finder_y == cur_y then
                neigh_data = neigh_data + 2
            end

            if finder_x == cur_x + 1 and finder_y == cur_y then
                neigh_data = neigh_data + 4
            end
        end

        wall:get("Tile"):set(neigh_data)
    end
end

return MapHandleSystem
