---
-- prepare_components.lua


Component.register(require "ecs.components.position")
Component.register(require "ecs.components.tile")
Component.register(require "ecs.components.map_entity")
Component.register(require "ecs.components.creature_entity")
Component.register(require "ecs.components.physics_entity")
Component.register(require "ecs.components.aim_entity")
Component.register(require "ecs.components.projectile_entity")
Component.register(require "ecs.components.player")
