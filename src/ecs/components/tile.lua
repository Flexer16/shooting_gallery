---
-- tile.lua


local class = require "middleclass"


local Tile = class("Tile")

function Tile:initialize(data)
    self.tile = nil

    if data then
        self.tile = data.tile
    end
end

function Tile:set(tile)
    self.tile = tile
end

function Tile:get()
    return self.tile
end

return Tile
