---
-- map_entity.lua


local class = require "middleclass"

local BaseEntity = require "ecs.components.base_entity"


local MapEntity = class("MapEntity", BaseEntity)

function MapEntity:initialize(data)
    BaseEntity.initialize(self, data)
end

return MapEntity
