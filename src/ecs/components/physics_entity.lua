---
-- physics_entity.lua


local class = require "middleclass"

local BaseEntity = require "ecs.components.base_entity"


local PhysicsEntity = class("PhysicsEntity", BaseEntity)

function PhysicsEntity:initialize(data)
    BaseEntity.initialize(self, data)

    self.target = {}
    self.target.x = nil
    self.target.y = nil
end

function PhysicsEntity:set_target(x, y)
    self.target.x = x
    self.target.y = y
end

function PhysicsEntity:is_target()
    if self.target.x and self.target.y then
        return true
    else
        return false
    end
end

function PhysicsEntity:remove_target()
    local x, y = self.target.x, self.target.y

    self.target.x = nil
    self.target.y = nil

    return x, y
end

return PhysicsEntity
