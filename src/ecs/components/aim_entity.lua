---
-- aim_entity.lua


local class = require "middleclass"

local BaseEntity = require "ecs.components.base_entity"


local AimEntity = class("AimEntity", BaseEntity)

function AimEntity:initialize(data)
    BaseEntity.initialize(self, data)
end

return AimEntity
