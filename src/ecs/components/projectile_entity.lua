---
-- projectile_entity.lua


local class = require "middleclass"

local BaseEntity = require "ecs.components.base_entity"


local ProjectileEntity = class("ProjectileEntity", BaseEntity)

function ProjectileEntity:initialize(data)
    BaseEntity.initialize(self, data)
end

return ProjectileEntity
