---
-- creature_entity.lua


local class = require "middleclass"

local BaseEntity = require "ecs.components.base_entity"


local CreatureEntity = class("CreatureEntity", BaseEntity)

function CreatureEntity:initialize(data)
    BaseEntity.initialize(self, data)
end

return CreatureEntity
