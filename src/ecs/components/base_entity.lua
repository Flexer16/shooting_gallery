---
-- base_entity.lua


local class = require "middleclass"


local BaseEntity = class("BaseEntity")

function BaseEntity:initialize(data)
    self.type = nil

    if data then
        self.type = data.type
    end
end

function BaseEntity:set_type(type)
    self.type = type
end

function BaseEntity:get_type()
    return self.type
end

function BaseEntity:is_type(type)
    if self.type == type then
        return true
    else
        return false
    end
end

return BaseEntity
