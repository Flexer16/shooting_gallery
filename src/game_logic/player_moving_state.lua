---
-- player_moving_state.lua


local class = require "middleclass"

local Actions = require "utils.enums.actions"


local PlayerMovingState = class("PlayerMovingState")

function PlayerMovingState:initialize(data)
    self.action = nil

    signal:register("kay-action", function (action) self.action = action end)
end

function PlayerMovingState:enter(owner)
    self.action = nil
end

function PlayerMovingState:execute(dt, owner)
    if self.action then
        local player_list = engine:getEntitiesWithComponent("Player")

        for _, player in pairs(player_list) do
            local cur_x, cur_y = player:get("Position"):get()

            local target_x, target_y = nil, nil

            if self.action == Actions.moving_up then
                target_x = cur_x
                target_y = cur_y - 1
            end

            if self.action == Actions.moving_down then
                target_x = cur_x
                target_y = cur_y + 1
            end

            if self.action == Actions.moving_left then
                target_x = cur_x - 1
                target_y = cur_y
            end

            if self.action == Actions.moving_right then
                target_x = cur_x + 1
                target_y = cur_y
            end

            player:get("PhysicsEntity"):set_target(target_x, target_y)

            if self.action == Actions.fire then
                local fsm = owner:get_fsm()
                fsm:change_state(owner:get_states().aim)
            end
        end

        self.action = nil
    end
end

function PlayerMovingState:exit(owner)
    self.action = nil
end

return PlayerMovingState