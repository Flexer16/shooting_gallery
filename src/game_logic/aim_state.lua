---
-- aim_state.lua


local class = require "middleclass"

local Actions = require "utils.enums.actions"


local AimState = class("AimState")

function AimState:initialize(data)
    self.action = nil

    signal:register("kay-action", function (action) self.action = action end)
    signal:register("chain-send-success", function () self:_chain_send_success() end)

    self.aim_chain_send_success = false
end

function AimState:enter(owner)
    self.action = nil
    self.aim_chain_send_success = false

    signal:emit("create-aim-data")
end

function AimState:execute(dt, owner)
    if self.action then
        if self.action == Actions.escape then
            local fsm = owner:get_fsm()
            fsm:change_state(owner:get_states().player_moving)
        end

        local dx, dy = nil, nil

        if self.action == Actions.moving_up then
            dx, dy = 0, -1
        end

        if self.action == Actions.moving_down then
            dx, dy = 0, 1
        end

        if self.action == Actions.moving_left then
            dx, dy = -1, 0
        end

        if self.action == Actions.moving_right then
            dx, dy = 1, 0
        end

        if dx and dy then
            signal:emit("move-aim", dx, dy)
        end

        if self.action == Actions.fire then
            signal:emit("send-aim-cahin")

            if self.aim_chain_send_success then
                local fsm = owner:get_fsm()
                fsm:change_state(owner:get_states().fire)
            end
        end

        self.action = nil
    end
end

function AimState:exit(owner)
    self.action = nil

    signal:emit("clear-aim-data")
end

function AimState:_chain_send_success()
    self.aim_chain_send_success = true
end

return AimState