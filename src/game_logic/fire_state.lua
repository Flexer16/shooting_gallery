---
-- fire_state.lua


local class = require "middleclass"


local FireState = class("FireState")

function FireState:initialize(data)
    self.return_to_move = false

    signal:register("fire-end", function () self:_to_move() end)
end

function FireState:enter(owner)
    -- body
end

function FireState:execute(dt, owner)
    if self.return_to_move then
        self.return_to_move = false

        local fsm = owner:get_fsm()
        fsm:change_state(owner:get_states().player_moving)
    end
end

function FireState:exit(owner)
    -- body
end

function FireState:_to_move()
    self.return_to_move = true
end

return FireState
