---
-- bresenham.lua


local class = require "middleclass"


local Bresenham = class("Bresenham")

function Bresenham:initialize(data)
    -- body
end

function Bresenham:line(funct, x_start, y_start, x_end, y_end)
    local step = math.abs(y_end - y_start) > math.abs(x_end - x_start)

    if step then
        x_start, y_start = y_start, x_start
        x_end, y_end = y_end, x_end
    end

    local delta_x, delta_y
    local increment

    if x_start > x_end then
        delta_x = x_start - x_end
        delta_y = math.abs(y_start - y_end)
        increment = -1
    else
        delta_x = x_end - x_start
        delta_y = math.abs(y_end - y_start)
        increment = 1
    end

    local err = 0
    local tmp_y = y_start
    local y_step

    if y_start < y_end then
        y_step = 1
    else
        y_step = -1
    end

    for tmp_x = x_start, x_end, increment do
        if step then
            funct(tmp_y, tmp_x)
        else
            funct(tmp_x, tmp_y)
        end

        err = err + delta_y

        if (err * 2) >= delta_x then
            tmp_y = tmp_y + y_step
            err = err - delta_x
        end
    end
end

return Bresenham
