---
-- map_tile_parser.lua


local class = require "middleclass"

local EntityFactory = require "utils.factories.entity_factory"


local MapTileParser = class("MapTileParser")

function MapTileParser:initialize()
    self.entity_factory = EntityFactory()
end

function MapTileParser:parse(tile, x, y)
    local map_tile = tile

    if tile == "@" then
        local player = self.entity_factory:get_creature_entity(tile, x, y)
        engine:addEntity(player)

        map_tile = "."
    end

    local map_entity = self.entity_factory:get_map_entity(map_tile, x, y)
    engine:addEntity(map_entity)
end

return MapTileParser
