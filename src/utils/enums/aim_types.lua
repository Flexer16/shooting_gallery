---
-- aim_types.lua


local AimTypes = {
    final_aim = "final_aim",
    regular_aim = "regular_aim",
    negative_aim = "negative_aim"
}

return AimTypes
