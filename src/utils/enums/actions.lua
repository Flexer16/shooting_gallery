---
-- actions.lua


local Actions = {
    moving_up = "moving_up",
    moving_down = "moving_down",
    moving_left = "moving_left",
    moving_right = "moving_right",
    fire = "fire",
    escape = "escape"
}

return Actions
