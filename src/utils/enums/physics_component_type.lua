---
-- physics_component_types.lua


local PhysicsComponentTypes = {
    static = "static",
    dynamic = "dynamic"
}

return PhysicsComponentTypes
