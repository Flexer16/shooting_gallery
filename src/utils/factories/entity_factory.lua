---
-- entity_factory.lua


local class = require "middleclass"

local MapTypes = require "utils.enums.map_types"
local CreatureTypes = require "utils.enums.creature_types"
local PhysicsComponentTypes = require "utils.enums.physics_component_type"
local AimTypes = require "utils.enums.aim_types"


local EntityFactory = class("EntityFactory")

function EntityFactory:initialize()

end

function EntityFactory:get_map_entity(map_tile, x, y)
    local Position, Tile, MapEntity, PhysicsEntity = Component.load({"Position", "Tile", "MapEntity", "PhysicsEntity"})

    local map_entity = Entity()
    map_entity:initialize()
    map_entity:add(Position({x = x, y = y}))
    map_entity:add(Tile({tile = map_tile}))

    local map_type = nil

    if map_tile == "#" then
        map_type = MapTypes.wall

        map_entity:add(PhysicsEntity({type = PhysicsComponentTypes.static}))
    elseif map_tile == "." then
        map_type = MapTypes.floor
    end

    map_entity:add(MapEntity({type = map_type}))

    return map_entity
end

function EntityFactory:get_creature_entity(creature_tile, x, y)
    local Position, Tile, CreatureEntity, PhysicsEntity, Player = Component.load({
        "Position",
        "Tile",
        "CreatureEntity",
        "PhysicsEntity",
        "Player"
    })

    local creature_entity = Entity()
    creature_entity:initialize()
    creature_entity:add(Position({x = x, y = y}))
    creature_entity:add(Tile({tile = creature_tile}))

    local creatyre_type = nil

    if creature_tile == "@" then
        creatyre_type = CreatureTypes.player

        creature_entity:add(Player())
    end

    creature_entity:add(CreatureEntity({type = creatyre_type}))
    creature_entity:add(PhysicsEntity({type = PhysicsComponentTypes.dynamic}))

    return creature_entity
end

function EntityFactory:get_finish_aim_entity(x, y)
    local Position, Tile, AimEntity = Component.load({"Position", "Tile", "AimEntity"})

    local aim_entity = Entity()
    aim_entity:initialize()
    aim_entity:add(Position({x = x, y = y}))
    aim_entity:add(Tile({tile = "final"}))
    aim_entity:add(AimEntity({type = AimTypes.final_aim}))

    return aim_entity
end

function EntityFactory:get_regular_aim_entity(x, y)
    local Position, Tile, AimEntity = Component.load({"Position", "Tile", "AimEntity"})

    local aim_entity = Entity()
    aim_entity:initialize()
    aim_entity:add(Position({x = x, y = y}))
    aim_entity:add(Tile({tile = "regular"}))
    aim_entity:add(AimEntity({type = AimTypes.regular_aim}))

    return aim_entity
end

function EntityFactory:get_negative_aim_entity(x, y)
    local Position, Tile, AimEntity = Component.load({"Position", "Tile", "AimEntity"})

    local aim_entity = Entity()
    aim_entity:initialize()
    aim_entity:add(Position({x = x, y = y}))
    aim_entity:add(Tile({tile = "negative"}))
    aim_entity:add(AimEntity({type = AimTypes.negative_aim}))

    return aim_entity
end

function EntityFactory:get_projectile(x, y)
    local Position, Tile, ProjectileEntity = Component.load({"Position", "Tile", "ProjectileEntity"})

    local projectile_entity = Entity()
    projectile_entity:initialize()
    projectile_entity:add(Position({x = x, y = y}))
    projectile_entity:add(Tile({tile = "flight"}))
    projectile_entity:add(ProjectileEntity())

    return projectile_entity
end

return EntityFactory
