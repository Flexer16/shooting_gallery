---
-- main.lua


package.path = package.path .. ";lib/?/init.lua;lib/?.lua;src/?.lua"


local lovetoys = require "lovetoys"
local Signal = require "hump.signal"


lovetoys.initialize({
    globals = true,
    debug = true
})


require "ecs.prepare_components"


local SceneHandleSystem = require "ecs.systems.scene_handle_system"
local ViewMapSystem = require "ecs.systems.view_map_system"
local ViewCreaturesSystems = require "ecs.systems.view_creatures_system"
local ViewAimSystem = require "ecs.systems.view_aim_system"
local ViewProjectilesSystem = require "ecs.systems.view_projectiles_system"
local MapHandleSystem = require "ecs.systems.map_handle_system"
local PhysicsSystem = require "ecs.systems.physics_system"
local UserInputSystem = require "ecs.systems.user_input_system"
local InputHandlerSystem = require "ecs.systems.input_handler_system"
local AimHandlerSystem = require "ecs.systems.aim_handler_system"
local ProjectileSystem = require "ecs.systems.projectiles_system"


function love.load()
    engine = Engine()
    signal = Signal.new()

    engine:addSystem(UserInputSystem(), "update")

    engine:addSystem(InputHandlerSystem(), "update")
    engine:addSystem(SceneHandleSystem(), "update")
    engine:addSystem(MapHandleSystem(), "update")
    engine:addSystem(PhysicsSystem(), "update")
    engine:addSystem(AimHandlerSystem(), "update")
    engine:addSystem(ProjectileSystem(), "update")

    engine:addSystem(ViewMapSystem(), "draw")
    engine:addSystem(ViewCreaturesSystems(), "draw")
    engine:addSystem(ViewAimSystem(), "draw")
    engine:addSystem(ViewProjectilesSystem(), "draw")

    signal:emit("load-new-scene", "res/maps/map.json")
end


function love.update(dt)
    if dt < 1/60 then
        love.timer.sleep(1/60 - dt)
    end

    engine:update(dt)
end


function love.draw()
    engine:draw()
end
